# Installation
> `npm install --save @types/es-abstract`

# Summary
This package contains type definitions for es-abstract (https://github.com/ljharb/es-abstract).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/es-abstract.

### Additional Details
 * Last updated: Wed, 18 Oct 2023 18:04:03 GMT
 * Dependencies: [@types/es-to-primitive](https://npmjs.com/package/@types/es-to-primitive)

# Credits
These definitions were written by [Jordan Harband](https://github.com/ljharb), and [ExE Boss](https://github.com/ExE-Boss).
